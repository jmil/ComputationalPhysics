### A Pluto.jl notebook ###
# v0.19.27

using Markdown
using InteractiveUtils

# ╔═╡ 02ba7cbc-468d-11ee-2deb-2d1be28da1c0
md"""
# Numberical Integration

There is a simple way of calculation of an integral: using the defenition of integration. For function $f(x)$, we have the integral

$I = \int_{a}^{b} f(x) \mathrm{d}x = \sum_{i=1}^{N} f(x_i) h$

Where $x_i = a + hi$. The key idea is that if we reduce the value of $h$, it'll be decrease the error $\Delta$, but if we continue, $\Delta$ will increase!

Lets do an experiment.
"""

# ╔═╡ ce049874-6be1-4b69-b846-186e047f9579
"integral of function `f` from `a` to `b`"
function integral(f::Function, a::Float64, b::Float64, N::Int64)
	h = (a+b)/N
	X = a:h:b
	I = sum(h*X)
end

# ╔═╡ 7ab92ecf-e2b5-4b97-991a-302dc5399917
f(x) = x.^2

# ╔═╡ 94a22398-cb45-40b9-8810-a564e58d7856
I = integral(f, 0.0, 2.0, 10)

# ╔═╡ Cell order:
# ╟─02ba7cbc-468d-11ee-2deb-2d1be28da1c0
# ╠═ce049874-6be1-4b69-b846-186e047f9579
# ╠═7ab92ecf-e2b5-4b97-991a-302dc5399917
# ╠═94a22398-cb45-40b9-8810-a564e58d7856
